import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HRPageComponent } from './pages/hrpage/hrpage.component';
import { DashboardCrmComponent } from './dashboard-crm/dashboard-crm.component';
import { AdminPageComponent } from './pages/admin-page/admin-page.component';
import { HrAdminPageComponent } from './pages/hr-admin-page/hr-admin-page.component';

const routes: Routes = [
  // { path: 'HRPage', component: HRPageComponent, pathMatch: 'full' },
  // { path: 'AdminPage', component: AdminPageComponent, pathMatch: 'full' },
  // { path: 'HrAdminPage', component: HrAdminPageComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }