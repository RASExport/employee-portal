import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HrAdminPageComponent } from './hr-admin-page.component';

describe('HrAdminPageComponent', () => {
  let component: HrAdminPageComponent;
  let fixture: ComponentFixture<HrAdminPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HrAdminPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HrAdminPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
