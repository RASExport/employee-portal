import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { AuthService } from '../../core/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-hr-admin-page',
  templateUrl: './hr-admin-page.component.html',
  styleUrls: ['./hr-admin-page.component.scss']
})
export class HrAdminPageComponent implements OnInit {

  userForm: FormGroup;
  formErrors = {
    'RASID': '',
    'password': ''
  };
  validationMessages = {
    'email': {
      'required': 'Please enter your RAS ID',
    },
    'password': {
      'required': 'Please enter your password',
      'pattern': 'The password must contain numbers and letters',
      'minlength': 'Please enter more than 4 characters',
      'maxlength': 'Please enter less than 25 characters',
    }
  };

  constructor(private router: Router,
    private fb: FormBuilder) {
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.userForm = this.fb.group({
      'RASID': ['', [
        Validators.required,
      ]
      ],
      'password': ['', [
        Validators.required,
      ]
      ],
    });

    this.userForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    // if (!this.userForm) {
    //   return;
    // }
    // const form = this.userForm;
    // for (const field in this.formErrors) {
    //   if (Object.prototype.hasOwnProperty.call(this.formErrors, field)) {
    //     this.formErrors[field] = '';
    //     const control = form.get(field);
    //     if (control && control.dirty && !control.valid) {
    //       const messages = this.validationMessages[field];
    //       for (const key in control.errors) {
    //         if (Object.prototype.hasOwnProperty.call(control.errors, key)) {
    //           this.formErrors[field] += messages[key] + ' ';
    //         }
    //       }
    //     }
    //   }
    // }
  }
  login() {
    // this.http.get('/Flights/getAircraftType').subscribe(c => {
    //   if (c != null) {
    //     if(){

    //     }
    //     else if(){

    //     }
    //     else{

    //     }
    //   }
    // },
    //   error => {

    //   });
  }
}
